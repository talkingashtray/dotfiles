set nocompatible
colorscheme slate
let mapleader = ","
"------------------------------------------------------------------------------
set conceallevel=2
set runtimepath+=
set encoding=utf-8
set ignorecase
set smartcase
set hidden
set go=a
set mouse=a
" search highlighting----------------------------------------------------------
"set hlsearch
set nohlsearch
set incsearch
augroup vim-incsearch-highlight
    autocmd!
    autocmd CmdlineEnter /,\? :set hlsearch
    autocmd CmdlineLeave /,\? :set nohlsearch
augroup END

set signcolumn=yes
set colorcolumn=80
augroup vim-nocolorcolumn
    autocmd!
    "autocmd BufEnter *.md,*.tex,*.bib,*.txt :set colorcolumn=
    autocmd filetype markdown,tex,bib,wiki,pandoc :set signcolumn=no
    autocmd filetype markdown,tex,bib,wiki,pandoc :set colorcolumn=
augroup END
" help options
" no shit with saved files-----------------------------------------------------
set noswapfile
set nobackup

" statusline-------------------------------------------------------------------
" can toggle some of that
"augroup vim-statusline
    "autocmd!
    "autocmd BufEnter * silent! :set laststatus=2
    "autocmd BufEnter * silent! :set statusline+=%<\ f:%F
    "autocmd BufEnter * silent! :set statusline+=\ %m
    "autocmd BufEnter * silent! :set statusline+=%=
    "autocmd BufEnter * silent! :set statusline+=\ w:%{wordcount().words}
        "autocmd BufEnter * silent! set statusline+=\ c: %o/%{line2byte(line('$')+1)-1} \
    "autocmd BufEnter * silent! :set statusline+=\ c:%{wordcount().chars}
    "autocmd BufEnter * silent! :set statusline+=\ l:%l/%L
    "autocmd BufEnter * silent! :set statusline+=\ p:%p%% "file Percentage
    "autocmd BufEnter * silent! :hi StatusLine ctermbg=0 ctermfg=15
"augroup END

"set laststatus=2
"set statusline+=%<\ f:%F
"set statusline+=\ %m
"set statusline+=%=
"set statusline+=\ w:%{wordcount().words}
    "set statusline+=\ c: %o/%{line2byte(line('$')+1)-1} \
"set statusline+=\ c:%{wordcount().chars}
"set statusline+=\ l:%l/%L
"set statusline+=\ p:%p%% "file Percentage
"hi StatusLine ctermbg=0 ctermfg=15

" line word wrapping-----------------------------------------------------------
" https://vim.fandom.com/wiki/Automatic_word_wrapping

autocmd BufEnter * silent! set wrap linebreak

" cursor highlight-------------------------------------------------------------
set cursorline
highlight cursorline cterm=none ctermbg=233 ctermfg=none

" tabs settings----------------------------------------------------------------
set tabstop=4
set smarttab
set shiftwidth=4
set expandtab

"---
set number relativenumber
set splitright splitbelow
"set wildmode=longest,list,full
set clipboard+=unnamedplus
syntax on
" invisible characters---------------------------------------------------------
set nolist
set listchars=tab:⇥\ ,eol:↵,space:·,trail:·
"set listchars=tab:⇥\ ,eol:¬ ¶
nmap <leader>l :set list!<CR>

" automatically change the current directory-----------------------------------
" https://vim.fandom.com/wiki/Set_working_directory_to_the_current_file
autocmd BufEnter * silent! lcd %:p:h
autocmd filetype markdown set syntax=pandoc

" filetypes--------------------------------------------------------------------
filetype on
autocmd filetype tex let g:tex_fold_enabled=1
autocmd filetype tex set fdm=syntax
autocmd filetype bib let g:tex_fold_enabled=1
autocmd filetype bib set fdm=syntax

" disable autocomment----------------------------------------------------------
autocmd filetype * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" remove whitespace trails-----------------------------------------------------
autocmd BufWritePre * let currPos = getpos(".")
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufWritePre *.[ch] %s/\%$/\r/e
autocmd BufWritePre * cal cursor(currPos[1], currPos[2])

" center doc in insert mode----------------------------------------------------
autocmd InsertEnter * norm zz

" replace slav no. sign with normal hash, mostly for md------------------------
autocmd filetype markdown,tex,wiki,pandoc imap № #
autocmd filetype markdown,tex,wiki,pandoc inoremap № #


" plugins ---------------------------------------------------------------------
" install vimplug and if not present
if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin()
    Plug 'lervag/wiki.vim'
    Plug 'lervag/lists.vim'
    Plug 'junegunn/fzf.vim'
    Plug '907th/vim-auto-save'
    Plug 'tpope/vim-fugitive'
    Plug 'lyokha/vim-xkbswitch'
    Plug 'vim-pandoc/vim-pandoc-syntax'
call plug#end()

" use ag or rg (silver search or ripgrep) as vimgrep --------------------------
set grepprg=ag\ --vimgrep
"set grepprg=rg\ --color=never\ --vimgrep
set grepformat^=%f:%l:%c:%m   " file:line:column:message
function! MySearch(grep_term)
    execute 'silent grep' a:grep_term | copen
endfunction
command! -nargs=+ Vg call MySearch(<q-args>)

autocmd VimLeave ~/docs/db/* :silent !rm -f *pdf


" templates--------------------------------------------------------------------
au BufNewFile ~/docs/db/**.md 0r ~/.config/nvim/templates/skeleton-wiki.md
            " \| <esc>/<++><enter>"_c4l abdts<esc>/<++><enter>"_c4l<c-r>%

" wiki.vim---------------------------------------------------------------------
let g:wiki_root = '~/docs/db'
let g:wiki_filetypes = ['md']
let g:wiki_ft_syntax = 'pandoc'     "gf ~/.config/nvim/init.vim
let g:wiki_link_extension = '.md'

let g:wiki_link_target_type = 'md'
let g:wiki_toc_title = 'contents:'
let g:wiki_template_title_month = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'now', 'dec']

let g:wiki_fzf_pages_opts = '--tac --algo=v2 --preview "cat {1}"'

" bibkeys
inoremap <expr> <c-x><c-w>r fzf#vim#complete('sed -e "s/@book{\(.*\),/•• \1/g" ~/docs/bibliography.bib \| grep "••" \| cut -d " " -f2' , {'window': { 'width': 0.2, 'height': 1, 'xoffset': 2 }})
" *i_CTRL-X_CTRL-E* -- scroll line up
" *i_CTRL-X_CTRL-Y* -- scroll line down

inoremap <expr> <c-x><c-l> fzf#vim#complete(fzf#wrap({
  \ 'prefix': '^.*$',
  \ 'source': 'rg -n ^ --color always',
  \ 'options': '--ansi --delimiter : --nth 3..',
  \ 'reducer': { lines -> join(split(lines[0], ':\zs')[2:], '') }}))

"nmap <leader>wss :WikiFzfPages<cr>
"nmap <leader>ww    <plug>(wiki-index)
"nmap <leader>wn    <plug>(wiki-open)
"nmap <leader>w<leader>w  <plug>(wiki-journal)
nmap <leader>jj     <plug>(wiki-journal)
nmap <leader>ji     <plug>(wiki-journal-index)
let g:wiki_mappings_local_journal = {
            \
            \ '<plug>(wiki-journal-prev)'   : '<leader>jp',
            \ '<plug>(wiki-journal-next)'   : '<leader>jn',
            \ '<plug>(wiki-journal-tomonth)': '<leader>jm',
            \ '<plug>(wiki-journal-toweek)' : '<leader>jw',
            \}
"TODO conflict w/ Vg search nmap <c-n>               <plug>(wiki-journal-prev)
"TODO conflict w/ Vg search nmap <c-p>               <plug>(wiki-journal-next)
"nmap <leader><c-n>       <plug>(wiki-journal-copy-tonext)
"nmap <leader>wu          <plug>(wiki-journal-toweek)
"nmap <leader>wm          <plug>(wiki-journal-tomonth)
"nmap <leader>wx          <plug>(wiki-reload)
"nmap <leader>wgb         <plug>(wiki-graph-find-backlinks)
"nmap <leader>wgc         <plug>(wiki-graph-check-links)
"nmap <leader>wgC         <plug>(wiki-graph-check-links-g)
"nmap <leader>wgi         <plug>(wiki-graph-in)
"nmap <leader>wgo         <plug>(wiki-graph-out)
"nmap <leader>wf          <plug>(wiki-link-toggle)
"nmap <leader>wd          <plug>(wiki-page-delete)
"nmap <leader>wr          <plug>(wiki-page-rename)
"nmap <leader>wt          <plug>(wiki-page-toc)
"nmap <leader>wT          <plug>(wiki-page-toc-local)
"nmap <leader>wp          <plug>(wiki-export)
"nmap <leader>wll         <plug>(wiki-link-show)
"nmap <leader>wlh         <plug>(wiki-link-extract-header)
"nmap <leader>wsl         <plug>(wiki-tag-list)
"nmap <leader>wsr         <plug>(wiki-tag-reload)
"nmap <leader>wss         <plug>(wiki-tag-search)
"nmap <leader>wss         <plug>(wiki-tag-search)
nmap <leader>wss         <plug>(wiki-fzf-tags)
"nmap <tab>               <plug>(wiki-link-next)
"nmap <cr>                <plug>(wiki-link-follow)
"nmap <c-w><cr>           <plug>(wiki-link-follow-split)
nmap <c-w><cr>j           <plug>(wiki-link-follow-split)
"nmap <c-w><tab>          <plug>(wiki-link-follow-vsplit)
nmap <c-w><cr>l          <plug>(wiki-link-follow-vsplit)
nmap <c-w><cr>t              <plug>(wiki-link-follow-tab)
"nmap <s-tab>             <plug>(wiki-link-prev)
"nmap <bs>                <plug>(wiki-link-return)
"nmap gl                  <plug>(wiki-link-toggle-operator)
nmap <leader>wnn <plug>(wiki-fzf-pages)
nmap <leader>wts <plug>(wiki-tag-search)
nmap <leader>we <esc>:silent! e `ls *md \\| shuf -n 1`<cr>


" vim regex to match things inside markdown links (single word: \[*\](\zs\w*.\ze.md)
"let g:wiki_tag_parsers = [
    "\ g:wiki#tags#default_parser,
    "\ { 'match': {x -> x =~# '^\?%\?\ \?tags: '},
    "\   'parse': {x -> split(matchstr(x, '^tags: \zs.*'), '[, ]\+ ')},
    "\   'make':  {t, x -> 'tags:' . empty(t) ? '' : join(t, ', ')}}
    "\]
" wiki.vim's original alternative parser
let g:wiki_tag_parsers = [
      \ g:wiki#tags#default_parser,
      \ { 'match': {x -> x =~# '^tags: '},
      \   'parse': {x -> split(matchstr(x, '^tags:\zs.*'), '[ ,]\+')},
      \   'make':  {t, x -> 'tags: ' . empty(t) ? '' : join(t, ', ')}}
      \]

" vim-pandoc-syntax wiki-related-----------------------------------------------
let g:pandoc#syntax#conceal#urls = 1
let g:pandoc#syntax#conceal#cchar_overrides = {"footnote": "^"}
"let g:pandoc#syntax#conceal#blacklist = ['list']

let g:wiki_tag_list = {'output': 'scratch'}

"auto-save---------------------------------------------------------------------
let g:auto_save = 0
augroup ft_markdown
au!
    au FileType markdown let b:auto_save = 1
    au FileType wiki let b:auto_save = 1
    let g:auto_save_silent = 1  " do not display the auto-save notification
augroup END

" fzf
let g:fzf_preview_window = ['right:50%', 'ctrl-/']
" keymaps
"
autocmd filetype * noremap <leader><Tab> <Esc>/<++><Enter>"_c4l
autocmd filetype * inoremap <leader><Tab> <Esc>/<++><Enter>"_c4l
autocmd filetype * vnoremap <leader><Tab> <Esc>/<++><Enter>"_c4l

autocmd filetype * noremap <leader><s-Tab> <Esc>?<++><Enter>"_c4l
autocmd filetype * inoremap <leader><s-Tab> <Esc>?<++><Enter>"_c4l
autocmd filetype * vnoremap <leader><s-Tab> <Esc>?<++><Enter>"_c4l

map <leader>p :!opout "%:p"<CR>
nmap <leader>f :call ToggleNetrw()<cr>
map <leader>c :w \| !compiler "%:p"<cr>

" dictionary-------------------------------------------------------------------

"set dictionary+=~/docs/dictionary.txt
" fzf replace the default dictionary completion with fzf-based fuzzy completion
"inoremap <expr> <c-x><c-k> fzf#vim#complete('cat ~/docs/dictionary.txt')
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'window': { 'width': 0.2, 'height': 0.9, 'xoffset': 1 }})

function! s:make_sentence(lines)
  return substitute(join(a:lines), '^.', '\=toupper(submatch(0))', '').'.'
endfunction

inoremap <expr> <c-x><c-s> fzf#vim#complete({
  \ 'source':  'cat ~/docs/dictionary.txt',
  \ 'reducer': function('<sid>make_sentence'),
  \ 'options': '--multi --reverse --margin 20%,0',
  \ 'right':    25})

" abbreviations----------------------------------------------------------------
"
" " netrw------------------------------------------------------------------------
" https://github.com/BrodieRobertson/dotfiles/blob/master/config/nvim/plugconfig/netrw.vim
" https://invidious.fdn.fr/watch?v=nDGhjk4Eqbc dedicated video

" this
let g:netrw_preview = 1
let g:netrw_keepdir = 0
let g:netrw_silent = 1
let g:netrw_mousemaps = 0
let g:netrw_sizestyle = "h"
let g:netrw_list_hide= '\(^\|\s\s\)\zs\.\S\+'

" netrw shit, stolen from brodie
let g:netrw_banner = 0
let g:netrw_liststyle = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 25
let g:netrw_bufsettings="noma nomod nonu nobl nowrap ro rnu"
let g:netrw_hide = 1

function! OpenToRight()
    :normal v
    let g:path=expand('%:p')
    execute 'q!'
    execute 'belowright vnew' g:path
    :normal <C-w>l
endfunction

function! OpenBelow()
    :normal v
    let g:path=expand('%:p')
    execute 'q!'
    execute 'belowright new' g:path
    :normal <C-w>l
endfunction

function! OpenTab()
   :normal v
   let g:path=expand('%:p')
   execute 'q!'
   execute 'tabedit' g:path
   :normal <C-w>l
endfunction

function! NetrwMappings()
    " Hack fix to make ctrl-l work properly
    noremap <buffer> <A-l> <C-w>l
    noremap <buffer> <C-l> <C-w>l
    noremap <silent> <A-f> :call ToggleNetrw()<CR>
    noremap <buffer> V :call OpenToRight()<cr>
    noremap <buffer> H :call OpenBelow()<cr>
    noremap <buffer> T :call OpenTab()<cr>
endfunction

augroup netrw_mappings
    autocmd!
    autocmd filetype netrw call NetrwMappings()
augroup END

"Allow for netrw to be toggled
function! ToggleNetrw()
   if g:NetrwIsOpen
       let i = bufnr("$")
       while (i >= 1)
           if (getbufvar(i, "&filetype") == "netrw")
               silent exe "bwipeout " . i
           endif
       let i-=1
       endwhile
   let g:NetrwIsOpen=0
   else
       let g:NetrwIsOpen=1
       "silent Lexplore
       " got this one from here: https://superuser.com/questions/1531456/how-to-reveal-a-file-in-vim-netrw-treeview
        :let @/=expand("%:t") | execute 'Lexplore' expand("%:h") | normal n<cr>
        "autocmd filetype netrw :normal
       endif
endfunction

"Check before opening buffer on any file
function! NetrwOnBufferOpen()
   if exists('b:noNetrw')
       return
   endif
   call ToggleNetrw()
endfun

"Close Netrw if it's the only buffer open
autocmd WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == "netrw" || &buftype == 'quickfix' |q|endif


"Make netrw act like a project Draw
augroup ProjectDrawer
   autocmd!
   " Don't open Netrw
       "autocmd VimEnter ~/.config/joplin/tmp/*,/tmp/calcurse*,~/.calcurse/notes/*,~/vimwiki/*,*/.git/COMMIT_EDITMSG let b:noNetrw=1
   autocmd VimEnter * :call NetrwOnBufferOpen() | :normal n
augroup END

let g:NetrwIsOpen=1
"----

let g:markdown_folding = 1

" open help window vertically to the left
"augroup vimrc_help
    "autocmd!
autocmd BufEnter *.txt if &buftype == 'help' | wincmd H | endif
"augroup END

" vim-xkbswitch settings-------------------------------------------------------
let g:XkbSwitchEnabled = 1
let g:XkbSwitchLib = '/usr/lib/libxkbswitch.so'
let g:XkbSwitchIMappings = ['uk']
"let g:XkbSwitchIMappingsTr = {
"                           \ 'uk':
"                           \ {'<': 'qwertyuiop[]asdfghjkl;''zxcvbnm,.`/'.
"                           \       'QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>?~@#$^&|',
"                           \ '>': "йцукенгшщзхїфівапролджэячсмитьбю\'.".
"                           \       'ЙЦУКЕНГШЩЗХЇФІВАПРОЛДЖЭЯЧСМИТЬБЮ,ʼ"№;:?/'}
"                           \ }

let g:XkbSwitchAssistNKeymap = 1    " for commands r and f
let g:XkbSwitchAssistSKeymap = 1    " for search lines
"let g:XkbSwitchKeymapNames = {'ru' : 'russian-jcuken', 'uk' : 'ukrainian-jcuken'}
"let g:XkbSwitchKeymapNames = {'ru' : 'ru_keymap', 'uk' : 'uk_keymap'}
let g:XkbSwitchDynamicKeymap = 1
let g:XkbSwitchNLayout = 'us'
let b:XkbSwitchILayout = 'us'
let g:XkbSwitchIMappingsSkipFt = ['tex']
"let g:XkbSwitchIMappingsTrData = '/home/arch/.config/nvim/plugged/vim-xkbswitch/xkbswitch.tr'

"-----

let g:lists_filetypes = ['md']
"let g:lists_maps_default_enable = v:false
"let g:lists_maps_default_override = {
      "\ 'i_<plug>(lists-new-element)': '',
      "\ 'i_<plug>(lists-toggle)': '<c-s>',
      "\ '<plug>(lists-toggle)': '<c-s>',
      "\}


" keymap to autocomplete tags
inoremap <expr> <c-x><c-w>t fzf#vim#complete("rg -IN --no-heading --color=never ^tags: ~/docs/db \| cut -d ' ' -f 2- \| tr ',' '\n' \| sed -e 's/  */ /' -e 's/^ //' \| sort \| uniq" ,{'window': { 'width': 0.3, 'height': 1, 'xoffset': 2 }})
inoremap <expr> <c-x><c-w>p fzf#vim#complete("ls",{'window': { 'width': 0.3, 'height': 1, 'xoffset': 2 }})
"autocmd filetype markdown iabbrev <expr>@time strftime("%Y.%m.%d, %R")

"autocmd filetype markdown iabbrev <buffer> @time strftime("%Y.%m.%d, %R")
"autocmd bufnewfile,buffilepre,bufread ~/docs/db/**.md iabbrev <expr>@time strftime("%Y.%m.%d, %R")

"autocmd filetype markdown iabbrev <expr> \time strftime("%F, %R")

"autocmd bufnewfile,buffilepre,bufread ~/docs/db/**.md :iabbrev <expr> @time strftime("%Y.%m.%d, %R")

" https://vim.fandom.com/wiki/Use_abbreviations_for_frequently-used_words
autocmd filetype markdown iabbrev <! <!-- <++> --><esc>
autocmd filetype markdown iabbrev <+ <++><esc>
"autocmd filetype * iabbrev ( (%)<esc>F%s<c-o>:call getchar()<CR>
"autocmd filetype * iabbrev [ [%]<esc>F%s<c-o>:call getchar()<CR>

" < and > - fix indenting visual block, stolen from brodie
vmap < <gv
vmap > >gv
" @- to repeat previous command
noremap @- :<c-p><c-p><cr>

iabbrev <expr> abdts strftime('%F, %R')
