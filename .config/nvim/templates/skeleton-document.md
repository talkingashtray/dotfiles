---
geometry:
- top=20mm
- left=30mm
- right=20mm
- bottom=20mm
fontsize: 14pt
linestretch: 1.5
mainfont: Liberation Serif
pagestyle: plain
mainfontoptions:
- Numbers=Lowercase
- Numbers=Proportional
linkcolor: blue
...
