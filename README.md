# my dotfiles

my workflow spins around humanities, that's why no codeshit

- dwm
    - no gaps and nonsence layouts
- alacritty/st
    - alacritty has no troubles with renderign diacritic signs like grave accents/acutes (а́, о́, у́, е́, etc. go test it yourself)
    - st as a backup
- zsh -- just werk
- neovim -- it's actually pretty usefull
    - wiki.vim (vimwiki in the past) to take notes
    - fzf/ripgrep to search through notes
- zathura
    - fzf-zathura to search everything readable

todo:

- [ ] comment script sources
- [ ] set up bootstrap
- [ ] look up for needless stuff
    - [ ] nvim config into single file
    - [ ] aliases/shortcuts
- [ ] stop tinkering
- [ ] reconfigure dunst to look less ugly
