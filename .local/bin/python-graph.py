#!/usr/bin/python

# https://stackoverflow.com/questions/67940820/how-to-extract-markdown-links-with-a-regex

import os
import regex as re
import pygraphviz as pgv

# regex patterns for links
pattern1 = re.compile(r'\[([^][]+)\](\(((?:[^(https:#)]+|(?2))+)\))') # for [description](url)
pattern2 = re.compile(r'\[\[([^][#]+)\|(([^][]+))\]\]')
pattern3 = re.compile(r'\[\[((([^][|#]+)))\]\]') # for [[url]] type

G = pgv.AGraph(strict=False, directed=False)

directory = '/home/arch/docs/db'
for filename in os.listdir(directory):
    if os.path.isfile(filename) and os.path.splitext(filename)[1] == ".md":
        basename = os.path.splitext(filename)[0]
        filedata = open(filename, "rt")
        dataread = filedata.read()
        G.add_node(basename, label="", xlabel=basename)
        #with open(filename, "rt",  encoding="utf-8", errors="ignore") as filedata:
        for match in pattern1.finditer(dataread):
            description, _, url = match.groups()
            G.add_node(url, label="", xlabel=basename)
            G.add_edge(basename, url)
        for match in pattern2.finditer(dataread):
            url, _, description = match.groups()
            #G.add_node(basename, label="", xlabel=basename)
            G.add_node(url, label="", xlabel=basename)
            G.add_edge(basename, url)
        for match in pattern3.finditer(dataread):
            url, _, description = match.groups()
            #G.add_node(basename, label="", xlabel=basename)
            G.add_node(url, label="", xlabel=basename)
            G.add_edge(basename, url)

G.graph_attr["overlap"] = "prism"
G.node_attr["shape"] = "circle"
G.layout(prog="sfdp")
G.write("noname.gv")
G.draw("noname.png", prog="sfdp")
#print(G)
